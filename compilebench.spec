Summary: Filesystem aging benchmark
Name: compilebench
Version: 0.6
Release: 1%{?dist}
License: GPL
Group: Utilities/Benchmarking
URL: http://oss.oracle.com/~mason/compilebench/
#Patch0: wc-custom.patch
Source: %{name}-%{version}.tar.gz
BuildRoot: /tmp/%{name}-buildroot

%description
Compilebench tries to age a filesystem by simulating some of the disk IO common in creating, compiling, patching, stating and reading kernel trees. It indirectly measures how well filesystems can maintain directory locality as the disk fills up and directories age. Thanks to Matt Mackall for the idea of simulating kernel compiles to achieve this.

%prep
%setup -q
#%patch0 -p1

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}%{_bindir} ${RPM_BUILD_ROOT}%{_datadir}/%{name}
install -m 0755 compilebench $RPM_BUILD_ROOT%{_bindir}
install -m 0644 dataset* $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%{_bindir}/dataset*

%changelog
* Wed Mar 23 2011 Michael MacDonald <mjmac@whamcloud.com>
- first packaging
