TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/compilebench-0.6.tar.gz compilebench-0.6
rpmbuild --define "_topdir $TOPDIR"  -bs compilebench.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/compilebench-0.6" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .